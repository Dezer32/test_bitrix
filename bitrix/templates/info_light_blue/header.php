<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
IncludeTemplateLangFile(__FILE__);
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= LANGUAGE_ID ?>" lang="<?= LANGUAGE_ID ?>">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="shortcut icon" type="image/x-icon" href="<?= SITE_TEMPLATE_PATH ?>/favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="<?= SITE_TEMPLATE_PATH ?>/common.css"/>
    <? $APPLICATION->ShowHead(); ?>
    <link rel="stylesheet" type="text/css" href="<?= SITE_TEMPLATE_PATH ?>/colors.css"/>
    <title><? $APPLICATION->ShowTitle() ?></title>
</head>
<body>
<div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
<div id="page-wrapper">

    <div id="page-body">
        <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
                <? $arCurDir = explode("/", $APPLICATION->GetCurDir()); ?>
                <td <? if (!array_search('forum', $arCurDir)): ?>width="60%"<? endif; ?> class="page-left">
                    <? if ($APPLICATION->GetCurDir() != SITE_DIR): ?>
                    <h1><? $APPLICATION->ShowTitle() ?></h1>
<? endif; ?>
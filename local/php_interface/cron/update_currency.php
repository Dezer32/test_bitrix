<?php
/**
 * Created by PhpStorm.
 * User: dezer
 * Date: 16.04.18
 * Time: 3:06
 */

//@todo не забыть проверить переменную окружения
require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';


use Bitrix\Main\Loader;

try {
    Loader::includeModule('cbr.parse');
} catch (\Bitrix\Main\LoaderException $e) {

    die();
}

\Cbr\Parse\UpdateApp::i()->updateRate();
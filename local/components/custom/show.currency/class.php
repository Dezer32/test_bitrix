<?php

use Bitrix\Main\Loader;
use Cbr\Parse\App;

/**
 * Created by PhpStorm.
 * User: dezer
 * Date: 16.04.18
 * Time: 3:16
 */

class ShowCurrency extends CBitrixComponent
{
    public function executeComponent()
    {

        try {
            Loader::includeModule('cbr.parse');
        } catch (\Bitrix\Main\LoaderException $e) {
            echo 111;
            die();
        }

        $cache = \Bitrix\Main\Data\Cache::createInstance();
        if ($cache->initCache(300, 'showCurrency')) {
            $this->arResult = $cache->getVars();
        } elseif ($cache->startDataCache()) {
            $this->arResult = App::i()->getAllCurrencyCodeValue();
            $cache->endDataCache($this->arResult);
        }


        if (isset($_REQUEST['sendMess'])) {
            $name = stripcslashes(htmlspecialchars($_REQUEST['name'])) ?? 'empty_name';
            $mail = stripcslashes(htmlspecialchars($_REQUEST['mail'])) ?? 'empty_mail';
            $sql = 'insert into send_message set UF_NAME = "'.$name.'", UF_MAIL = "'.$mail.'", UF_TIME = '.time().', UF_RATE = \''.serialize($this->arResult).'\'';
            \Bitrix\Main\Application::getConnection()->query($sql);

            mail($mail, 'Курс валют', print_r($this->arResult, true));
        }

        $this->includeComponentTemplate();
    }
}
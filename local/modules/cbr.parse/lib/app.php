<?php
/**
 * Created by PhpStorm.
 * User: dezer
 * Date: 16.04.18
 * Time: 1:37
 */

namespace Cbr\Parse;


use Bitrix\Main\Application;
use Bitrix\Main\DB\Exception;
use Bitrix\Main\Db\SqlQueryException;
use Bitrix\Main\Config\Configuration as Config;
use CIBlockElement;
use CUtil;

class App extends AbstractAppBase
{
    static protected $_instance;

    protected $connect;

    /**
     * App constructor.
     */
    public function __construct()
    {
        $this->connect = Application::getConnection();
    }

    public static function i()
    {
        if (self::$_instance != null) {
            return self::$_instance;
        }

        return new self;
    }

    function getAllCurrencyCodeValue() {
        $result = false;
        $sql = 'select bie.CODE, cr.UF_CURRENT_VALUE from b_iblock_element bie join currency_rate cr on (cr.UF_CURRENCY_ID = bie.ID)';

        $params = ['func' => 'getAllCurrencyCodeValue'];
        if ($result = $this->getDataFromCache($params)) {
            return $result;
        }

        try {
            $result = $this->connect->query($sql)->fetchAll();
        } catch (SqlQueryException $e) {
            $this->error($e);
        }

        return $result;
    }

    /**
     * @param $name
     * @param $code
     * @param null $numCode
     * @return bool|array
     * @throws Exception
     */
    function setCurrencyName($name, $code, $numCode = null)
    {
        // проблема с кодировкой на хосте, поэтому пока так
        $name = base64_encode($name);
        $ibElem = new CIBlockElement();
        $result = $ibElem->Add([
            'IBLOCK_ID' => Config::getValue('IBCurrencyName'),
            'NAME' => $name,
            'CODE' => $code,
            'PROPERTY_VALUES' => [
                'num_code' => $numCode
            ]
        ]);
        if (!$result) {
            throw new Exception('Не удалось добавить элемент');
        }
        return $result;
    }

    function getCurrencyRate($id)
    {
        $result = false;
        $params = ['func' => 'getCurrencyRate', 'id' => $id];

        if ($result = $this->getDataFromCache($params)) {
            return $result;
        }
        $sql = 'SELECT * from currency_rate where uf_currency_id = ' . $id;
        try {
            $result = $this->connect->query($sql)->fetch();
        } catch (SqlQueryException $e) {
            $this->error($e);
        }
        $this->setDataToCache($params, $result);
        return $result;
    }

    function updateCurrencyRate($id, $oldValue, $currentValue)
    {
        if (!$this->getCurrencyRate($id)) {
            $this->createCurrencyRate($id, $currentValue);
        }
        $sql = 'update currency_rate set UF_OLD_VALUE = ' . $oldValue . ', UF_CURRENT_VALUE = ' . $currentValue . ' UF_TIME = ' . time() . ' where UF_CURRENCY_ID = ' . $id;

        try {
            $this->connect->query($sql);
        } catch (SqlQueryException $e) {
            $this->error($e);
        }
    }

    function createCurrencyRate($id, $currentValue)
    {
        $sql = 'insert into currency_rate set UF_OLD_VALUE = 0, UF_CURRENT_VALUE = ' . $currentValue . ', UF_TIME = ' . time() . ', UF_CURRENCY_ID = ' . $id;
        try {
            $this->connect->query($sql);
        } catch (SqlQueryException $e) {
            $this->error($e);
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: dezer
 * Date: 16.04.18
 * Time: 1:42
 */

namespace Cbr\Parse;


use Bitrix\Main\Data\Cache;

abstract class AbstractAppBase
{
    protected $cache;
    protected $ttl = 24 * 60 * 60;

    /**
     * AbstractAppBase constructor.
     */
    public function __construct()
    {
        $this->cache = Cache::createInstance();
    }

    protected function setDataToCache($params, $data)
    {
//        $this->cache->startDataCache($this->ttl, serialize($params), false, $data);
    }

    protected function getDataFromCache($params)
    {
//        $result = false;
//        if ($this->cache->initCache($this->ttl, serialize($params))) {
//            $result = $this->cache->getVars();
//        }
//        return $result;
    }

    protected function error($data)
    {
        echo '<pre>' . print_r($data, true) . '</pre>';
        die();
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: dezer
 * Date: 16.04.18
 * Time: 0:06
 */

namespace Cbr\Parse\Api;


use Exception;
use SoapClient;

class CBRApi implements ICBRApi
{

    const DAILY = 'http://www.cbr.ru/scripts/XML_daily.asp';

    static function getCursOnTime($timestamp)
    {
        $params = [
            'date_req' => date('d/m/Y', $timestamp)
        ];
        return (new self())->send(self::DAILY, $params);
    }

    function send($url, $params)
    {
        $data = [];

        try {
            $xml = simplexml_load_file($url . '?' . http_build_query($params));
            $json = json_encode($xml);
            $data = json_decode($json, true);
        } catch (Exception $e) {
            $data = ['error' => $e->getMessage()];
        }

        return $data;
    }
}
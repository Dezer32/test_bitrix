<?php
/**
 * Created by PhpStorm.
 * User: dezer
 * Date: 16.04.18
 * Time: 0:04
 */

namespace CBR\Parse\Api;


interface ICBRApi
{
    function send($url, $params);
}
<?php
/**
 * Created by PhpStorm.
 * User: dezer
 * Date: 16.04.18
 * Time: 0:56
 */

namespace Cbr\Parse;


use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Application;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\Config\Configuration as Config;
use Bitrix\Main\Data\Cache;
use Bitrix\Main\DB\Exception;
use Bitrix\Main\Loader;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\SystemException;
use Cbr\Parse\Api\CBRApi;
use CIBlockElement;

class UpdateApp extends AbstractAppBase
{

    static protected $_instance;

    /**
     * UpdateApp constructor.
     */
    public function __construct()
    {
        Loader::includeModule('iblock');
        parent::__construct();
    }


    public static function i()
    {
        if (self::$_instance != null) {
            return self::$_instance;
        }

        return new self;
    }

    function getCurrencyNameId($code)
    {
        $result = [];
        $params = [
            'select' => [
                'ID'
            ],
            'filter' => [
                'CODE' => $code,
                'IBLOCK_ID' => Config::getValue('IBCurrencyName')
            ]
        ];

        if ($result = $this->getDataFromCache($params)) {
            return $result;
        }

        try {
            $result = ElementTable::getList($params)->fetch();
        } catch (ObjectPropertyException $e) {
            $this->error($e);
        } catch (ArgumentException $e) {
            $this->error($e);
        } catch (SystemException $e) {
            $this->error($e);
        }

        $this->setDataToCache($params, $result);

        return $result['ID'];
    }


    protected function getOrSetCurrencyId($name, $code, $numCode = null)
    {
        $params = ['func' => 'getOrSetCurrencyId', 'code' => $code];
        if ($result = $this->getDataFromCache($params)) {
            return $result;
        }

        if (empty($result = $this->getCurrencyNameId($code))) {
            try {
                $result = App::i()->setCurrencyName($name, $code, $numCode);
            } catch (\Exception $e) {
            }
        }

        $this->setDataToCache($params, $result);

        return $result;
    }

    function updateRate()
    {
        $rate = CBRApi::getCursOnTime(time());
        if (!empty($rate['error'])) {
            $this->error($rate['error']);
            return false;
        }

        foreach ($rate['Valute'] as $currency) {
            $currency['Value'] = str_replace(',', '.', $currency['Value']);
            if ($idCurrency = $this->getOrSetCurrencyId($currency['Name'], $currency['CharCode'],
                $currency['NumCode'])) {
                if ($currentCurrency = App::i()->getCurrencyRate($idCurrency)) {
                    if ($currentCurrency['UF_CURRENT_VALUE'] != $currency['Value']) {
                        App::i()->updateCurrencyRate($idCurrency, $currentCurrency['UF_CURRENT_VALUE'],
                            $currency['VALUE']);
                    }
                } else {
                    App::i()->createCurrencyRate($idCurrency, $currency['Value']);
                }
            }
        }
    }
}